#include <iostream> 
#include <stdlib.h>
#include <vector>
#include <fstream>

#include "linkedList.h"

using namespace std;

/* @Author Mehmet Baysan
Student Name: Metin Ozkan Ergin
Student ID : 504201578
Date: 26.10.2021 */

int main(int argc, char **argv) {
	//system("clear");// make this line as comment if you are compiling on Linux or Mac
	system("cls"); // make this line as comment if you are compiling on Windows

	LinkedList newLinkedList;

	// READING FROM FILE
	//std::string input_file = argv[1];

	//std::string reader;
	//std::ifstream file_reader(input_file);
	//if (file_reader.is_open())
	//{
	//	while (getline(file_reader, reader))
	//	{
	//		char a = reader[0];
	//		newLinkedList.addElement(a);
	//		newLinkedList.size++;
	//	}
	//	file_reader.close();
	//}

	newLinkedList.addElement('D');
	newLinkedList.addElement('B');
	newLinkedList.addElement('B');
	newLinkedList.addElement('A');
	newLinkedList.addElement('A');
	newLinkedList.addElement('C');
	newLinkedList.addElement('A');
	newLinkedList.addElement('U');
	newLinkedList.addElement('G');
	newLinkedList.addElement('Y');
	newLinkedList.size = 10;

	cout << "Readed letters in linked list: ";
	newLinkedList.printList();

	newLinkedList.reorderList();
	cout << "After reordering: ";
	newLinkedList.printList();

	newLinkedList.removeDublicates();
	cout << "After removing dublicates: ";
	newLinkedList.printList();

	newLinkedList.reverseList();
	cout << "Reversed list: ";
	newLinkedList.printList();


	return EXIT_SUCCESS;
}