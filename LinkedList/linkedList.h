#ifndef _H
#define _H

#include <iostream>
#include <stdio.h>


using namespace std;

/* @Author Mehmet Baysan
Student Name: Metin Ozkan Ergin
Student ID : 504201578
Date: 26.10.2021 */


class Node{
    public:
        char letter;
        Node* next;
        Node(char);
        ~Node();
        void set_data(char);
        char get_data();
        void set_next(Node*);
        Node* get_next();
};

class LinkedList{

    Node* head ;
    Node* tail ;

    public:
        LinkedList();
        void reorderList();
        void removeDublicates();
        void reverseList();
        void printList();
        void addElement(char);
        int size;       // For using bubble sort algorithm.
};

#endif