#include <iostream>
#include <stdio.h>

#include "linkedList.h"

using namespace std;

/* @Author Mehmet Baysan
Student Name: Metin Ozkan Ergin
Student ID : 504201578
Date: 26.10.2021 */

void LinkedList::reorderList(){
    //FILL THIS FUNCTION ACCORDINGLY
    // It is bubble sort algorithm
     Node* first_node  = head;
     Node* second_node = head;

       for (int i = 0; i < size; i++) 
       {
        for (int j = 0; j < size - 1; j++) 
        {
            if (first_node->letter < second_node->letter) 
            {
                char temp = first_node->letter;                  // This part changes the letter of two nodes.
                first_node->letter = second_node->letter;
                second_node->letter = temp;
            }
            second_node = second_node->next;        // Iterate the another node.
        }
        second_node = head;             
        first_node = first_node->next;
    }
};

void LinkedList::removeDublicates(){
    //FILL THIS FUNCTION ACCORDINGLY
    // It is like bubble sort algorithm. It can be used like bubble sort but I did different type of searching.
    Node* current = head;
    while (current != NULL) 
    {
        Node* head_next = current->next;
        Node* tail = current;
        while (head_next != NULL) 
        {
            if (head_next->letter == current->letter) {
                // duplicated item found.
                tail->next = head_next->next;   // tail does not show head anymore. It shows head's next.
            }
            else
            tail = tail->next;
            head_next = head_next->next;
            }
        current = current->next;    // Let's iterate one more from beginning.
        }
};

void LinkedList::reverseList(){
    //FILL THIS FUNCTION ACCORDINGLY 
     Node *temporary = NULL;     // For changing order of nodes
     Node *previous = NULL;
     Node *head_node = head;
    while(head_node != NULL) 
    {
        temporary = head_node->next;    // Keep the head node next.
        head_node->next = previous;     // Change head node next.
        previous = head_node;        
        head_node = temporary;          // Head node set with keeping node.
    }
    head = previous;
};

void LinkedList::printList(){
    Node* temp = head;
    while(temp != NULL){
        cout<<temp->letter<<" ";
        temp = temp->next;
    }
    cout<<endl;
};

LinkedList::LinkedList(){
    head = NULL;
    tail = NULL;
}

void LinkedList::addElement(char input)
{
        Node *tmp = new Node(input);
        tmp->next   = NULL;

        if(head == NULL)
        {
            head = tmp;
            tail = tmp;
        }
        else
        {
            tail->next = tmp;
            tail = tail->next;
        }
}


Node::Node(char data){
    this->letter = data;
    this->next = NULL;
}

Node::~Node(){
    if(this->next != NULL)
        delete this->next;
}

void Node::set_data(char data){
    this->letter=data;
}

char Node::get_data(){
    return this->letter;
}

void Node::set_next(Node* next){
    this->next=next;
}

Node* Node::get_next(){
    return this->next;
}